#!/bin/bash

# Função para imprimir uma linha com caracteres específicos
linha() {
    local caracter=$1
    local tamanho=$2

    for ((i = 1; i <= tamanho; i++)); do
        echo -n "$caracter"
    done

    echo   # Adiciona uma nova linha no final
}

# Função para imprimir uma coluna com caracteres específicos
coluna() {
    local caracter=$1
    local tamanho=$2

    for ((i = 1; i <= tamanho; i++)); do
        echo "$caracter"
    done
}

# Exemplos de uso das funções
echo "Linha de asteriscos:"
linha "*" 10

echo -e "\nColuna de cifrões:"
coluna "$" 5
