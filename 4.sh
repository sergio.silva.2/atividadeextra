#!/bin/bash

# Inclui as funções do arquivo func.sh
source func.sh

# Solicita ao usuário que insira o nome do arquivo
read -p "Digite o nome do arquivo: " nome_arquivo

# Verifica se o arquivo existe
if [ -e "$nome_arquivo" ]; then
    # Solicita ao usuário que escolha entre linha ou coluna
    read -p "Escolha 'linha' ou 'coluna': " escolha

    # Executa a escolha do usuário
    case $escolha in
        linha)
            # Solicita ao usuário que insira o número da linha
            read -p "Digite o número da linha: " numero_linha
            # Exibe a i-ésima linha do arquivo
            linha "$numero_linha" "$nome_arquivo"
            ;;
        coluna)
            # Solicita ao usuário que insira o número da coluna
            read -p "Digite o número da coluna: " numero_coluna
            # Exibe a i-ésima coluna do arquivo
            coluna "$numero_coluna" "$nome_arquivo"
            ;;
        *)
            echo "Escolha inválida. Por favor, escolha 'linha' ou 'coluna'."
            ;;
    esac
else
    echo "Erro: O arquivo '$nome_arquivo' não existe."
fi
