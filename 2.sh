#Escreva a função linha. Esta deve receber um número (i) como argumento e deve exibir a i-ésima linha de um arquivo (cujo nome também foi passado como argumento. Ex.: linha 10 /etc/passwd

 #!/bin/bash

# Função para exibir a i-ésima linha de um arquivo
linha() {
    local linha_numero=$1
    local arquivo=$2

    # Verifica se o arquivo existe
    if [ -e "$arquivo" ]; then
        # Exibe a i-ésima linha do arquivo
        sed -n "${linha_numero}p" "$arquivo"
    else
        echo "Erro: O arquivo '$arquivo' não existe."
    fi
}

# Exemplo de uso da função
linha 10 /etc/passwd
