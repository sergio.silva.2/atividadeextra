#!/bin/bash

# Função para exibir a i-ésima coluna de um arquivo
coluna() {
    local coluna_numero=$1
    local arquivo=$2

    # Verifica se o arquivo existe
    if [ -e "$arquivo" ]; then
        # Exibe a i-ésima coluna do arquivo (usando ':' como delimitador)
        awk -F: "{print \$$coluna_numero}" "$arquivo"
    else
        echo "Erro: O arquivo '$arquivo' não existe."
    fi
}

# Exemplo de uso da função
coluna 2 /etc/passwd
